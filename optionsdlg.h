#ifndef OPTIONSDLG_H
#define OPTIONSDLG_H

#include <QWidget>
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>
#include <QList>
#include <QListWidget>
#include <QColorDialog>
#include <QFileDialog>
#include <QFileInfo>
#include <QTextStream>

#include "comandsstructure.h"
#include "qcolorbutton.h"
#include "testlabthread.h"

namespace Ui {
class optionsDlg;
}

class optionsDlg : public QWidget
{
    Q_OBJECT

public:
    explicit optionsDlg(QWidget *parent = 0);
    ~optionsDlg();

    void setCommandsList(QList<commandsStructure> *);

public slots:
    void hideProgressBar();
    void updateList();
    void doubleclick();
    void updateLabInfo(int exito, int fallo, int total );
    void infoLabFail(QString msg);
    void finalizadoLab();

signals:
    void loadImage(QString File);
    void resetToOriginal();
    void want2Leave();
    void toGrayImage();
    void calcContours();
    void calcApproxContours(double detail);
    void toGaussianBlurEffect(int size,double sigma, int type);
    void toMedianBlurEffect(int size);
    void toBlurEffect(int size,double anchor, int type);
    void toBoxFilterEffect(int ddepth, int size,double anchor, bool normalize, int type);
    void calcThreshold(double thresh, double max, int type);
    void calcAdaptativeThreshold(double max, int method, int type, int blocksize, double c);
    void applyContrastandBrigth(double contrast, int brigth);
    void findCornersCanny( double threshold1, double threshold2, int apertureSize, bool L2gradient );
    void findCornersEigen( int blockSize, int ksize, int borderType );
    void denoisingNlMeansColored(float h, float hColor, int templateWindowSize, int searchWindowSize);
    void setOldImage(cv::Mat image);
    void equalizeHist();
    void inRange(QColor lowColor, QColor highColor);
    void morphologyEx(int op, double anchor, int iter, int bordertype);
    void method1();
    void method2();
    void method3();
    void UseMethod1();
    void combThreshold();
    void HoughLinesP(double rho, double theta, int threshold, double minLineLength, double maxLineGap);
    void Sobel(int ddepth, int dx, int dy, int ksize, double scale, double delta, int borderType);
    void Scharr(int ddepth, int dx, int dy, double scale, double delta, int borderType);
    void SobelCombined();
    void promedios();
    void UserMethodCombined();

private slots:
    void on_grayBtn_clicked();

    void on_contourBtn_clicked();

    void on_openBtn_clicked();

    void on_applyGBtn_clicked();

    void on_applyMBtn_clicked();

    void on_applyNBtn_clicked();

    void on_applyBFBtn_clicked();

    void on_appleNTBtn_clicked();

    void on_appleATBtn_clicked();

    void on_applyCBtn_clicked();

    void on_applyEVAVBtn_clicked();

    void on_applyMDCBtn_clicked();

    void on_applyCABtn_clicked();

    void on_resetBtn_clicked();

    void on_quitBtn_clicked();

    void on_applyBCBtn_clicked();

    void on_getFileBtn_clicked();

    void on_eqHistBtn_clicked();

    void on_saveLogBtn_clicked();

    void on_applyIRBtn_clicked();

    void on_applyMBtn_2_clicked();

    void on_method1Btn_clicked();

    void on_applyHLBtn_clicked();

    void on_applySobelBtn_clicked();

    void on_applyScharrBtn_clicked();

    void on_SobelCombBtn_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_usingmehtod1Btn_clicked();

    void on_thresholdCombBtn_clicked();

    void on_runBtn_clicked();

    void on_getFolderInBtn_clicked();

    void on_setFolderOutBtn_clicked();

    void on_meansBtn_clicked();

    void on_usingFinalMethodBtn_clicked();

private:
    Ui::optionsDlg *ui;
    QList<commandsStructure> *acciones;
    QColorButton *lowIRBtn, *highIRBtn;
    TestLabThread *hilo;
};

#endif // OPTIONSDLG_H
