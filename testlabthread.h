#ifndef TESTLABTHREAD_H
#define TESTLABTHREAD_H

#include <QThread>
#include <QStringList>
#include <QAbstractAnimation>
#include <QString>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QImage>
#include <QElapsedTimer>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <cmath>
#include <string>
#include <vector>

class TestLabThread : public QThread
{
    Q_OBJECT
public:
    TestLabThread(QObject *parent = 0);
    ~TestLabThread();
    void analyseFolder(QString folderin, QString folderout, int method);

signals:
    void updateInfo(int exito, int fallo, int total );
    void infoFail(QString msg);
    void finalizado();
protected:
    void run() override;
private:
    QFileInfoList imagesFiles;
    QFile *logF, *dbF;
    int method, exitosos, fallidos;
    std::string folderout,good, bad;

    void analyseImage(QFileInfo file, cv::Mat (TestLabThread::*function)(cv::Mat));
    void analyseImage(QFileInfo file, std::vector<std::vector<cv::Point>> (TestLabThread::*metodo)(cv::Mat), void (TestLabThread::*metododebug)(cv::Mat&));

    cv::Mat method1(cv::Mat img);
    cv::Mat method2(cv::Mat img);
    cv::Mat method3(cv::Mat img);
    cv::Mat method4(cv::Mat img);
    cv::Mat method5(cv::Mat img);
    cv::Mat method6(cv::Mat img);

    std::vector<std::vector<cv::Point>> combinedMethods1(cv::Mat img);
    void combinedMethodsLog1(cv::Mat &img);
    std::vector<std::vector<cv::Point>> combinedMethods2(cv::Mat img);
    void combinedMethodsLog2(cv::Mat &img);
    std::vector<std::vector<cv::Point>> combinedMethods3(cv::Mat img);
    void combinedMethodsLog3(cv::Mat &img);
    std::vector<std::vector<cv::Point>> combinedMethods4(cv::Mat img);
    void combinedMethodsLog4(cv::Mat &img);

    void saveImage(cv::Mat, QFileInfo namesrc, bool isgood);

    cv::Mat ThresholdingCombiner(cv::Mat img1, cv::Mat img2);
    uchar combine(uchar color1, uchar color2);
    std::vector<std::vector<cv::Point>> reCalculateSquares(std::vector<std::vector<cv::Point>> c, cv::Mat m);
    bool stdDevisOK(cv::Scalar standard);
    bool meanisOK(cv::Scalar media);
    bool stdDevisOKLog(cv::Scalar standard);
    bool meanisOKLog(cv::Scalar media);
    bool almostSquare(std::vector<cv::Point> quad);
    cv::Point getCenter(std::vector<cv::Point> quad);
    std::vector<cv::Point> orderCenters(cv::Point c1, cv::Point cw, cv::Point c3);
    double calcAngleBetween(cv::Point a, cv::Point b, cv::Point c);
    cv::Point complementaryPoint(cv::Point a, cv::Point b, cv::Point c);
    cv::Point growPoint(double grow, cv::Point v, cv::Point w, cv::Point p);
    std::vector<cv::Point> calcularActa(double grow, cv::Point a, cv::Point b, cv::Point c, cv::Point d);
    void calcTransformationsPoints(cv::Point2f *vert, std::vector<cv::Point> centros, cv::Point2f *src, cv::Point2f *dest, int size=3);
    void removeDuplicateSquares(std::vector<std::vector<cv::Point>> *quads);
    std::vector<std::vector<cv::Point>> obtainSquares(cv::Mat imagen, cv::Mat srcimg);
    void obtainSquaresLog(cv::Mat imagen, cv::Mat &srcimg);
    int minimumStdDevSquares(std::vector<std::vector<cv::Point>> &cuadros, cv::Mat srcimg);
    cv::Point calculatePerspectiveFourthPoint(std::vector<std::vector<cv::Point>> squares, std::vector<cv::Point> centers);
    std::vector<cv::Point> calculatePerspectiveActa(std::vector<std::vector<cv::Point>> squares, std::vector<cv::Point> &centers, cv::Mat img);
    std::vector<std::vector<cv::Point>> reCalculateSquaresP(std::vector<std::vector<cv::Point>> c, cv::Mat M);
};

#endif // TESTLABTHREAD_H
