#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle("OpenCV Lab");

    QDockWidget *dockSpace = new QDockWidget("Herramientas",this);
    dockSpace->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    dock = new optionsDlg(dockSpace);
    dockSpace->setWidget(dock);

    opencvWindow = new ventana();

    setCentralWidget(opencvWindow);
    addDockWidget(Qt::RightDockWidgetArea,dockSpace);

    resize(800,600);

    connect(dock,SIGNAL(loadImage(QString)),SLOT(openFile(QString)));
    connect(dock,SIGNAL(applyContrastandBrigth(double,int)),opencvWindow,SLOT(BrightnessContrast(double,int)));
    connect(dock,SIGNAL(calcAdaptativeThreshold(double,int,int,int,double)),opencvWindow,SLOT(AdaptativeThresholding(double,int,int,int,double)));
    connect(dock,SIGNAL(calcApproxContours(double)),opencvWindow,SLOT(getApproxContours(double)));
    connect(dock,SIGNAL(calcContours()),opencvWindow,SLOT(getContours()));
    connect(dock,SIGNAL(calcThreshold(double,double,int)),opencvWindow,SLOT(Thresholding(double,double,int)));
    connect(dock,SIGNAL(denoisingNlMeansColored(float,float,int,int)),opencvWindow,SLOT(denoisingNlMeansColored(float,float,int,int)));
    connect(dock,SIGNAL(findCornersCanny(double,double,int,bool)),opencvWindow,SLOT(Canny(double,double,int,bool)));
    connect(dock,SIGNAL(findCornersEigen(int,int,int)),opencvWindow,SLOT(cornerEigenValsAndVecs(int,int,int)));
    connect(dock,SIGNAL(resetToOriginal()),opencvWindow,SLOT(back2Source()));
    connect(dock,SIGNAL(toBlurEffect(int,double,int)),opencvWindow,SLOT(Blur(int,double,int)));
    connect(dock,SIGNAL(toBoxFilterEffect(int,int,double,bool,int)),opencvWindow,SLOT(BoxFilter(int,int,double,bool,int)));
    connect(dock,SIGNAL(toGaussianBlurEffect(int,double,int)),opencvWindow,SLOT(GaussianBlur(int,double,int)));
    connect(dock,SIGNAL(toGrayImage()),opencvWindow,SLOT(GrayImage()));
    connect(dock,SIGNAL(toMedianBlurEffect(int)),opencvWindow,SLOT(MedianBlur(int)));
    connect(dock,SIGNAL(equalizeHist()),opencvWindow,SLOT(equalizeHist()));
    connect(dock,SIGNAL(inRange(QColor,QColor)),opencvWindow,SLOT(InRange(QColor,QColor)));
    connect(dock,SIGNAL(morphologyEx(int,double,int,int)),opencvWindow,SLOT(morphologyEx(int,double,int,int)));
    connect(dock,SIGNAL(method1()),opencvWindow,SLOT(method1()));
    connect(dock,SIGNAL(method2()),opencvWindow,SLOT(method2()));
    connect(dock,SIGNAL(method3()),opencvWindow,SLOT(method3()));
    connect(dock,SIGNAL(UseMethod1()),opencvWindow,SLOT(UseMethod1()));
    connect(dock,SIGNAL(HoughLinesP(double,double,int,double,double)),opencvWindow,SLOT(HoughLinesP(double,double,int,double,double)));
    connect(dock,SIGNAL(Sobel(int,int,int,int,double,double,int)),opencvWindow,SLOT(Sobel(int,int,int,int,double,double,int)));
    connect(dock,SIGNAL(Scharr(int,int,int,double,double,int)),opencvWindow,SLOT(Scharr(int,int,int,double,double,int)));
    connect(dock,SIGNAL(SobelCombined()),opencvWindow,SLOT(SobelCombined()));
    connect(dock,SIGNAL(combThreshold()),opencvWindow,SLOT(combineThreshold()));
    connect(dock,SIGNAL(promedios()),opencvWindow,SLOT(meanTest()));
    connect(dock,SIGNAL(UserMethodCombined()),opencvWindow,SLOT(UseMethodsCombined()));

    connect(dock,SIGNAL(want2Leave()),SLOT(close()));
    connect(dock,SIGNAL(resetToOriginal()),SLOT(resetImage()));
    connect(opencvWindow,SIGNAL(changeImage(commandsStructure)),SLOT(changeActions(commandsStructure)));
    connect(dock,SIGNAL(setOldImage(cv::Mat)),opencvWindow,SLOT(showImage(cv::Mat)));

    dock->setCommandsList(&proceso);

}

MainWindow::~MainWindow(){}

void MainWindow::openFile(QString file)
{
    commandsStructure comando;
    cv::Mat imagen = cv::imread(file.toStdString());

    comando.setMessage("Abrir Imagen:"+file);
    comando.setImage(imagen);
    opencvWindow->loadImage(comando.getImage());
    proceso.clear();
    proceso.append(comando);
    dock->updateList();
}

void MainWindow::resetImage()
{
    commandsStructure first;
    if (proceso.size() > 0)
    {
        first=proceso.first();
        proceso.clear();
        proceso.append(first);
        dock->updateList();
    }
}

void MainWindow::changeActions(commandsStructure action)
{
    proceso.append(action);
    dock->hideProgressBar();
    dock->updateList();
}
