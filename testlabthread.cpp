#include "testlabthread.h"

using std::vector;

const double PI = 3.141592653589793;

TestLabThread::TestLabThread(QObject *parent) : QThread(parent)
{
    imagesFiles.clear();
    logF = NULL;
    dbF = NULL;
    good = "bien";
    bad  = "mal";
    method = -1;
    exitosos = 0;
    fallidos = 0;
}

TestLabThread::~TestLabThread()
{
    imagesFiles.clear();
    wait();
    if (logF != NULL)
    {
        logF->close();
        delete logF;
    }
    if (dbF != NULL)
    {
        dbF->close();
        delete dbF;
    }
}

void TestLabThread::analyseFolder(QString folderin, QString folderout, int method)
{
    QStringList filters;
    QDir dir(folderin), dirout(folderout);

    filters << "*.png" << "*.jpg" << "*.bmp";
    imagesFiles = dir.entryInfoList(filters, QDir::Files|QDir::NoDotAndDotDot);
    this->folderout = folderout.toStdString();

    if(imagesFiles.size() > 0 && dirout.mkpath(QString(good.c_str())) && dirout.mkpath(QString(bad.c_str())))
    {
        if (logF != NULL)
            delete logF;
        logF = new QFile(folderout+"/output.txt");

        if (dbF != NULL)
            delete dbF;
        dbF = new QFile(folderout+"/db.cvs");

        if (logF->open(QFile::WriteOnly) && dbF->open(QFile::WriteOnly))
        {
            this->method = method;
            start(QThread::LowPriority);
        }
    }
    else
    {
        emit updateInfo(0,0,0);
        emit finalizado();
    }
}

void TestLabThread::run()
{
    cv::Mat (TestLabThread::*function)(cv::Mat);
    QFileInfo image;
    QTextStream log(logF);
    QElapsedTimer timer;
    int tiempo;

    if (method == 1)
        function = &TestLabThread::method1;
    else if (method == 2)
        function = &TestLabThread::method2;
    else if (method == 3)
        function = &TestLabThread::method3;
    else if (method == 4)
        function = &TestLabThread::method4;
    else if (method == 5)
        function = &TestLabThread::method5;
    else if (method == 6)
        function = &TestLabThread::method6;

    exitosos = 0;
    fallidos = 0;

    timer.start();

    if (method < 7)
    {
        foreach (image, imagesFiles)
        {
            analyseImage(image,function);
        }
    }
    else if (method == 7)
    {
        foreach (image, imagesFiles)
        {
            analyseImage(image,&TestLabThread::combinedMethods1, &TestLabThread::combinedMethodsLog1);
        }
    }
    else if (method == 8)
    {
        foreach (image, imagesFiles)
        {
            analyseImage(image,&TestLabThread::combinedMethods2, &TestLabThread::combinedMethodsLog2);
        }
    }
    else if (method == 9)
    {
        foreach (image, imagesFiles)
        {
            analyseImage(image,&TestLabThread::combinedMethods3, &TestLabThread::combinedMethodsLog3);
        }
    }
    else if (method == 10)
    {
        foreach (image, imagesFiles)
        {
            analyseImage(image,&TestLabThread::combinedMethods4, &TestLabThread::combinedMethodsLog4);
        }
    }
    tiempo = ((double)timer.elapsed())/1000;
    log << endl;
    log << "Total: " << imagesFiles.size() << " Exitosas: " << exitosos << "(" << (double)(exitosos*100)/((double)imagesFiles.size()) << "%) Fallos: " << fallidos << "("  << (double)(fallidos*100)/((double)imagesFiles.size()) << "%)" << endl;
    log << "Tiempo: " << tiempo/60 << " minutos " << tiempo%60 << " segundos" << endl;
    emit finalizado();
}

void TestLabThread::analyseImage(QFileInfo filesrc, cv::Mat (TestLabThread::*function)(cv::Mat))
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx, centros, acta;
    vector<vector<cv::Point>> tempList,cuadros;
    vector<cv::Vec4i> hierarchy;
    cv::Mat canvas, pos, mask, element;
    cv::RotatedRect actaFinal;
    cv::Point2f srcTri[3], destTri[3], vertActa[4];
    cv::Size af_Size;
    cv::Scalar cmean, stddev;
    cv::Mat imagen;
    int cont,i, idx;
    double area, areamin, areamax;


    QTextStream log(logF);
    QTextStream db(dbF);

    cv::Mat srcimg = cv::imread(filesrc.absoluteFilePath().toStdString());

    imagen = (*this.*function)(srcimg);

    cv::findContours(imagen,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    areamin = srcimg.size().width*srcimg.size().height*0.00024;
    areamax = srcimg.size().width*srcimg.size().height*0.0034;

    cuadros = obtainSquares(imagen, srcimg);
    removeDuplicateSquares(&cuadros);
    if (cuadros.size() > 3)
    {
        tempList.clear();
        for(i=0; i < 3; i++)
        {
            idx = minimumStdDevSquares(cuadros,srcimg);

            tempList.push_back(cuadros[idx]);
            cuadros.erase(cuadros.begin()+idx);
        }
        cuadros = tempList;
    }
    if (cuadros.size() == 3)
    {
        // Ordenamos los centros
        centros = orderCenters(getCenter(cuadros[0]), getCenter(cuadros[1]), getCenter(cuadros[2]));
        centros.push_back(complementaryPoint(centros[0], centros[1], centros[2]));

        acta = calcularActa(0.017,centros[0], centros[1], centros[2], centros[3]);
        actaFinal = cv::minAreaRect(acta);
        actaFinal.points(vertActa);
        calcTransformationsPoints(vertActa,centros,srcTri,destTri);
        pos = cv::getAffineTransform(srcTri,destTri);

        af_Size.width = cv::norm(destTri[0] - destTri[1]);
        af_Size.height = cv::norm(destTri[0] - destTri[2]);

        cv::warpAffine(srcimg,canvas,pos,af_Size);

        cuadros = reCalculateSquares(cuadros, pos);
        cv::drawContours(canvas,cuadros,-1,cv::Scalar(25,220,62),3);

        log << filesrc.baseName() << " ........ OK" <<  endl;
        db << filesrc.baseName() << ", OK" << endl;
        saveImage(canvas,filesrc,true);
        exitosos++;

    }
    else
    {
        log << filesrc.baseName() << " ........ FAIL -- cuadros: " << cuadros.size() << endl;
        db << filesrc.baseName() << ", FAIL" << endl;
        cv::Scalar color;
        cv::Point centro;
        for(i=0; i < contours.size() ; i++)
        {
            tempList.clear();
            cv::approxPolyDP(contours[i],approx,0.024*(cv::arcLength(contours[i],true)),true);
            tempList.push_back(approx);
            area = cv::contourArea(approx);
            if (approx.size() == 4  && cv::isContourConvex(approx) && area > areamin && area < areamax && almostSquare(approx) )
            {
                centro = getCenter(approx);
                color = cv::Scalar(43,25,220);
                mask = cv::Mat::zeros( srcimg.size(), CV_8U );
                cv::drawContours(mask,tempList, 0, 255, -1);
                element = cv::getStructuringElement(0,cv::Size(6,6));
                cv::morphologyEx(mask,mask,cv::MORPH_ERODE,element);
                cmean = cv::mean(srcimg,mask);
                cv::meanStdDev(srcimg,cmean,stddev,mask);
                log << "\tC("<< centro.x << "," << centro.y << ")\tArea: " << area << " ";
                if (stdDevisOKLog(stddev) && meanisOKLog(cmean)){
                    //qDebug() << "area=" << area;
                    color = cv::Scalar(25,220,62);
                    log << "..... OK" << endl;
                }
                else{
                    log << "..... FAIL" << endl;
                }
                cv::drawContours( srcimg, tempList, 0, color, 3, 8, hierarchy, 0, cv::Point() );
            }
        }
        saveImage(srcimg,filesrc,false);
        fallidos++;
        emit infoFail(filesrc.baseName()+" encontré: "+QString::number(cuadros.size())+" cuadros.");
    }
    emit updateInfo(exitosos,fallidos,imagesFiles.size());
}

void TestLabThread::analyseImage(QFileInfo filesrc, std::vector<std::vector<cv::Point>> (TestLabThread::*metodo)(cv::Mat), void (TestLabThread::*metododebug)(cv::Mat&))
{
    vector<cv::Point> centros, acta;
    vector<vector<cv::Point>> cuadros;
    cv::Mat canvas, pos;
    //cv::RotatedRect actaFinal;
    cv::Point2f srcF[4], destF[4], vertActa[4];
    cv::Size af_Size;
    int i;


    QTextStream log(logF);
    QTextStream db(dbF);

    cv::Mat srcimg = cv::imread(filesrc.absoluteFilePath().toStdString());

    cuadros = (*this.*metodo)(srcimg);

    if (cuadros.size() == 3)
    {
        // Ordenamos los centros
        centros = orderCenters(getCenter(cuadros[0]), getCenter(cuadros[1]), getCenter(cuadros[2]));
        /*centros.push_back(complementaryPoint(centros[0], centros[1], centros[2]));

        acta = calcularActa(0.017,centros[0], centros[1], centros[2], centros[3]);
        actaFinal = cv::minAreaRect(acta);
        actaFinal.points(vertActa);
        calcTransformationsPoints(vertActa,centros,srcTri,destTri);
        pos = cv::getAffineTransform(srcTri,destTri);

        af_Size.width = cv::norm(destTri[0] - destTri[1]);
        af_Size.height = cv::norm(destTri[0] - destTri[2]);

        cv::warpAffine(srcimg,canvas,pos,af_Size);

        cuadros = reCalculateSquares(cuadros, pos);
        cv::drawContours(canvas,cuadros,-1,cv::Scalar(25,220,62),3);*/

        acta = calculatePerspectiveActa(cuadros,centros,srcimg);
        for(i=0; i < acta.size(); i++)
        {
            vertActa[i] = acta.at(i);
        }
        calcTransformationsPoints(vertActa,centros,srcF,destF,4);
        pos = cv::getPerspectiveTransform(srcF,destF);

        af_Size.width = cv::norm(destF[0] - destF[1]);
        af_Size.height = cv::norm(destF[0] - destF[2]);

        cv::warpPerspective(srcimg,canvas,pos,af_Size);
        cuadros = reCalculateSquaresP(cuadros, pos);
        cv::drawContours(canvas,cuadros,-1,cv::Scalar(25,220,62),3);

        log << filesrc.baseName() << " ........ OK" <<  endl;
        db << filesrc.baseName() << ", OK" << endl;
        saveImage(canvas,filesrc,true);
        exitosos++;

    }
    else
    {
        log << filesrc.baseName() << " ........ FAIL -- cuadros: " << cuadros.size() << endl;
        db << filesrc.baseName() << ", FAIL" << endl;
        (*this.*metododebug)(srcimg);
        saveImage(srcimg,filesrc,false);
        fallidos++;
        emit infoFail(filesrc.baseName()+" encontré: "+QString::number(cuadros.size())+" cuadros.");
    }
    emit updateInfo(exitosos,fallidos,imagesFiles.size());
}

std::vector<std::vector<cv::Point>> TestLabThread::combinedMethods1(cv::Mat img)
{
    int idx,i;
    cv::Mat base, output, element;
    vector<vector<cv::Point>> cuadros, temp;

    cv::cvtColor( img, base, CV_BGR2GRAY );
    cv::blur(base,base,cv::Size(5,5));
    cv::threshold(base,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    cuadros.clear();
    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    cv::threshold(base,output,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    if (cuadros.size() > 3)
    {
        temp.clear();
        for(i=0; i < 3; i++)
        {
            idx = minimumStdDevSquares(cuadros,img);

            temp.push_back(cuadros[idx]);
            cuadros.erase(cuadros.begin()+idx);
        }
    }
    else
    {
        temp.clear();
        temp = cuadros;
    }

    return temp;
}

void TestLabThread::combinedMethodsLog1(cv::Mat &img)
{
    cv::Mat base, output, element;

    cv::cvtColor( img, base, CV_BGR2GRAY );
    cv::blur(base,base,cv::Size(5,5));
    cv::threshold(base,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);

    cv::threshold(base,output,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);
}

std::vector<std::vector<cv::Point>> TestLabThread::combinedMethods2(cv::Mat img)
{
    int idx,i;
    cv::Mat base, output, element;
    vector<vector<cv::Point>> cuadros, temp;

    cv::cvtColor( img, base, CV_BGR2GRAY );
    cv::blur(base,base,cv::Size(5,5));
    cv::threshold(base,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    cuadros.clear();
    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    cv::threshold(base,output,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    cv::threshold(base,output,48,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    if (cuadros.size() > 3)
    {
        temp.clear();
        for(i=0; i < 3; i++)
        {
            idx = minimumStdDevSquares(cuadros,img);

            temp.push_back(cuadros[idx]);
            cuadros.erase(cuadros.begin()+idx);
        }
    }
    else
    {
        temp.clear();
        temp = cuadros;
    }

    return temp;
}

void TestLabThread::combinedMethodsLog2(cv::Mat &img)
{
    cv::Mat base, output, element;

    cv::cvtColor( img, base, CV_BGR2GRAY );
    cv::blur(base,base,cv::Size(5,5));
    cv::threshold(base,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);

    cv::threshold(base,output,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);

    cv::threshold(base,output,48,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);
}

std::vector<std::vector<cv::Point>> TestLabThread::combinedMethods3(cv::Mat img)
{
    int idx,i;
    cv::Mat base, output, element;
    vector<vector<cv::Point>> cuadros, temp;

    cv::cvtColor( img, base, CV_BGR2GRAY );
    cv::blur(base,base,cv::Size(5,5));
    cv::threshold(base,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    cuadros.clear();
    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    cv::threshold(base,output,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    cv::threshold(base,output,122,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    if (cuadros.size() > 3)
    {
        temp.clear();
        for(i=0; i < 3; i++)
        {
            idx = minimumStdDevSquares(cuadros,img);

            temp.push_back(cuadros[idx]);
            cuadros.erase(cuadros.begin()+idx);
        }
    }
    else
    {
        temp.clear();
        temp = cuadros;
    }

    return temp;
}

void TestLabThread::combinedMethodsLog3(cv::Mat &img)
{
    cv::Mat base, output, element;

    cv::cvtColor( img, base, CV_BGR2GRAY );
    cv::blur(base,base,cv::Size(5,5));
    cv::threshold(base,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);

    cv::threshold(base,output,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);

    cv::threshold(base,output,122,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);


}

std::vector<std::vector<cv::Point>> TestLabThread::combinedMethods4(cv::Mat img)
{
    int idx,i;
    cv::Mat base, output, element;
    vector<vector<cv::Point>> cuadros, temp;

    cv::cvtColor( img, base, CV_BGR2GRAY );
    cv::blur(base,base,cv::Size(5,5));
    cv::threshold(base,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    cuadros.clear();
    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    cv::threshold(base,output,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    cv::threshold(base,output,122,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    cv::threshold(base,output,48,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, img);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    if (cuadros.size() > 3)
    {
        temp.clear();
        for(i=0; i < 3; i++)
        {
            idx = minimumStdDevSquares(cuadros,img);

            temp.push_back(cuadros[idx]);
            cuadros.erase(cuadros.begin()+idx);
        }
    }
    else
    {
        temp.clear();
        temp = cuadros;
    }

    return temp;
}

void TestLabThread::combinedMethodsLog4(cv::Mat &img)
{
    cv::Mat base, output, element;

    cv::cvtColor( img, base, CV_BGR2GRAY );
    cv::blur(base,base,cv::Size(5,5));
    cv::threshold(base,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);

    cv::threshold(base,output,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);

    cv::threshold(base,output,122,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);

    cv::threshold(base,output,48,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    obtainSquaresLog(output, img);

}


cv::Mat TestLabThread::method1(cv::Mat img)
{
    cv::Mat output;
    cv::cvtColor( img, output, CV_BGR2GRAY );
    cv::blur(output,output,cv::Size(5,5));
    cv::threshold(output,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    cv::Mat element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    return output;
}

cv::Mat TestLabThread::method2(cv::Mat img)
{

    cv::Mat imgBlur, threshold1, threshold2, thresholdfinal,element;
    cv::cvtColor( img, imgBlur, CV_BGR2GRAY );
    cv::GaussianBlur(imgBlur,imgBlur,cv::Size(5,5),0);

    cv::threshold(imgBlur,threshold1,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(threshold1,threshold1,cv::MORPH_CLOSE,element);

    cv::threshold(imgBlur,threshold2,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(threshold2,threshold2,cv::MORPH_OPEN,element,cv::Point(-1,-1),2);

    thresholdfinal = ThresholdingCombiner(threshold1,threshold2);

    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(thresholdfinal,thresholdfinal,cv::MORPH_CLOSE,element);

    return thresholdfinal;
}

cv::Mat TestLabThread::method3(cv::Mat img)
{
    cv::Mat output;
    cv::cvtColor( img, output, CV_BGR2GRAY );
    cv::blur(output,output,cv::Size(5,5));
    cv::threshold(output,output,48,255,cv::THRESH_BINARY);
    cv::Mat element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    return output;
}

cv::Mat TestLabThread::method4(cv::Mat img)
{
    cv::Mat output;
    cv::cvtColor( img, output, CV_BGR2GRAY );
    cv::blur(output,output,cv::Size(5,5));
    cv::threshold(output,output,105,255,cv::THRESH_BINARY);
    cv::Mat element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    return output;
}

cv::Mat TestLabThread::method5(cv::Mat img)
{
    cv::Mat output;
    cv::cvtColor( img, output, CV_BGR2GRAY );
    cv::blur(output,output,cv::Size(5,5));
    cv::threshold(output,output,122,255,cv::THRESH_BINARY);
    cv::Mat element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    return output;
}

cv::Mat TestLabThread::method6(cv::Mat img)
{
    cv::Mat output;
    cv::cvtColor( img, output, CV_BGR2GRAY );
    cv::GaussianBlur(output,output,cv::Size(5,5),0);
    cv::medianBlur(output,output,5);
    cv::adaptiveThreshold(output,output,255,cv::ADAPTIVE_THRESH_GAUSSIAN_C,cv::THRESH_BINARY,11,1);

    return output;
}

void TestLabThread::saveImage(cv::Mat img, QFileInfo srcFile, bool isgood)
{
    QTextStream log(logF);
    cv::Mat nimage;

    cv::cvtColor(img,nimage,CV_BGR2RGB);
    QImage imgIn= QImage((uchar*) nimage.data, nimage.cols, nimage.rows, nimage.step, QImage::Format_RGB888);

    if(isgood)
    {

        if (!imgIn.save(QString((folderout + "/" + good + "/" + srcFile.baseName().toStdString() + ".png").c_str())))
        {
            log << "ERROR: No se pudo guardar el archivo: " << QString((folderout + "/" + good + "/" + srcFile.baseName().toStdString() + ".png").c_str()) << endl;
        }
    }
    else
    {
        if (!imgIn.save(QString((folderout + "/" + bad + "/" + srcFile.baseName().toStdString() + ".png").c_str())))
        {
            log << "ERROR: No se pudo guardar el archivo: " << QString((folderout + "/" + bad + "/" + srcFile.baseName().toStdString() + ".png").c_str()) << endl;
        }
    }

}

cv::Mat TestLabThread::ThresholdingCombiner(cv::Mat img1, cv::Mat img2){
    int i,j;
    cv::Mat imgf = img1.clone();

    int cols = img1.cols, rows = img1.rows;
    if(img1.isContinuous())
    {
        cols *= rows;
        rows = 1;
    }

    for (i=0; i < rows; i++){

        const uchar* M1i = img1.ptr<uchar>(i);
        const uchar* M2i = img2.ptr<uchar>(i);
        uchar* Mfi = imgf.ptr<uchar>(i);

        for(j=0; j < cols; j++){
            Mfi[j] = combine(M1i[j],M2i[j]);
        }
    }

    return imgf;

}

uchar TestLabThread::combine(uchar color1, uchar color2){
    if (color1 == color2 ){
        return color1;
    }
    else{
        return 255;
    }

}

bool TestLabThread::stdDevisOK(cv::Scalar standard)
{
    bool res = true;
    for(int i =0; i < 3; i++)
    {
        res = res && standard[i] < 27;
    }
    return res;
}

bool TestLabThread::meanisOK(cv::Scalar media)
{
    bool res = true;
    int min=255, max=0;
    for(int i =0; i < 3; i++)
    {
        if (max < media[i]) max = media[i];
        if (media[i] < min) min = media[i];
        res = res && media[i] < 128;
    }
    if (res)
    {
        res = res && (max - min) < 30;
    }

    return res;
}

bool TestLabThread::stdDevisOKLog(cv::Scalar standard)
{
    bool res = true;
    QTextStream log(logF);
    log << "stdDev = ( ";
    for(int i =0; i < 3; i++)
    {
        res = res && standard[i] < 27;
        log << " " << standard[i];
    }
    log << ") ";
    return res;
}

bool TestLabThread::meanisOKLog(cv::Scalar media)
{
    bool res = true;
    int min=255, max=0;
    QTextStream log(logF);

    log << "media = (";
    for(int i =0; i < 3; i++)
    {
        if (max < media[i]) max = media[i];
        if (media[i] < min) min = media[i];
        res = res && media[i] < 128;
        log << " " << media[i];
    }
    log << ") ";
    if (res)
    {
        res = res && (max - min) < 25;
        log << " distmean = " << (max - min)<< " ";
    }

    return res;
}

bool TestLabThread::almostSquare(vector<cv::Point> quad)
{
    vector<cv::Point> edge;
    double size1, size2, finalsize1, tol= 0.1, ratio;
    bool ok=false;
    //Primeros lados opuestos
    edge.push_back(quad[0]);
    edge.push_back(quad[1]);
    size1 = cv::arcLength(edge,false);
    edge.clear();
    edge.push_back(quad[2]);
    edge.push_back(quad[3]);
    size2 = cv::arcLength(edge,false);
    ok = std::abs(size1-size2) < size1*tol;
    if (ok)
    {
        finalsize1 = (size1+size2);
        //Segundos lados opuestos
        edge.clear();
        edge.push_back(quad[1]);
        edge.push_back(quad[2]);
        size1 = cv::arcLength(edge,false);
        edge.clear();
        edge.push_back(quad[3]);
        edge.push_back(quad[0]);
        size2 = cv::arcLength(edge,false);
        ok = std::abs(size1-size2) < size1*tol;
        if (ok)
        {
            size2 = (size1+size2);
            ratio = finalsize1/size2; //size2 y finalsize1 eran divididos entre dos para ser el promedio, pero en esta divición se cancela el 2, por lo que no tiene sentido hacer la división
            ok = ratio > 0.8 && ratio < 1.25;
        }
    }
    return ok;
}

double TestLabThread::calcAngleBetween(cv::Point a, cv::Point b, cv::Point c)
{
    /**
    *       Función que calcula en ángulo entre dos segmentos (a-b) y (c-b).
    *       El ángulo va desde 0 hasta 6.2657320147 radianes aprox (0-359 grados)
    *
    *       Entrada
    *           a,b,c: JSON {x,y} puntos que definen los segmentos con un punto en común (b).
    *
    *       Salida
    *           angle: Número el ángulo entre los segmentos.
    *
    *       Autor: Gustavo Adolfo García Cano
    *       email: gustavo.garcia@ine.mx
    **/
    cv::Point v, w;
    double det, prod, angle;
    v = a - b;
    w = c - b;
    angle = -1;
    if (std::abs(v.x+v.y) > 1e-12 || std::abs(w.x+w.y) > 1e-12){
        det = v.x*w.y - v.y*w.x;
        prod = w.x*v.x + v.y*w.y;
        angle = std::atan2(det,prod);
        if (std::abs(angle) > 1e-12 && angle < 0){
            angle = 2*PI + angle;
        }
    }
    return angle;
}

cv::Point TestLabThread::getCenter(vector<cv::Point> quad)
{
    cv::Point2d centro, A1, A2, B1, B2;
    double alpha=0,tan=0, div=0;;

    A1 = quad[0];
    A2 = quad[2]-quad[0];

    B1 = quad[1];
    B2 = quad[3]-quad[1];

    if (std::abs(A2.x+A2.y) > 1e-16 || std::abs(B2.x+B2.y) > 1e-16)
    {
        if (std::abs(B2.x) > 1e-16)
        {
            tan = B2.y/B2.x;
            div = A2.y - A2.x*tan;
            if (std::abs(div) > 1e-16)
                alpha = (B1.y-A1.y - (B1.x-A1.x)*tan)/div;
            else
                alpha = -5;
        }
        else
        {
            tan = B2.x/B2.y;
            div = (A2.x - A2.y*tan);
            if (std::abs(div) > 1e-16)
                alpha = (B1.x-A1.x - (B1.y - A1.y)*tan)/div;
            else
                alpha = -5;
        }
    }

    if (alpha == -5)
    {
        //qDebug() << "ERROR no encontré el centro";
    }
    else
    {
        centro = A1 + alpha*A2;
    }


    return centro;
}

std::vector<cv::Point> TestLabThread::orderCenters(cv::Point c1, cv::Point c2, cv::Point c3)
{
    double ladoMax, lado, m, dist,angbetween;
    int Ai;
    vector<cv::Point> centros;
    cv::Point pointT;

    ladoMax= cv::norm(c1-c2);
    Ai = 0;

    lado = cv::norm(c2-c3);
    if (lado > ladoMax){
        ladoMax = lado;
        Ai = 1;
    }

    lado = cv::norm(c3-c1);
    if (lado > ladoMax){
        ladoMax = lado;
        Ai = 2;
    }

    /**
        *     Una vez que tenemos el lado más largo, este está formado por los puntos A y C
        *     Lo renombramos como: centro1 = A, centro2 = C, centro3 = B, por lo tanto
        *     centro3 será el vertice opuesto a la hipotenusa
        **/
    switch (Ai) {
    case 0:
        Ai = 2;
        break;
    case 1:
        pointT = c3;
        c3 = c1;
        c1 = pointT;
        pointT = c2;
        c2 = c1;
        c1 = pointT;
        break;
    case 2:
        pointT = c2;
        c2 = c1;
        c1 = pointT;
        pointT = c3;
        c3 = c1;
        c1 = pointT;
        break;
    }

    /// Pendiente de la hipotenusa
    m = (double)(c2.y - c1.y)/(double)(c2.x - c1.x);
    /// Distancia del vértice a la hipotenusa (con orientación)
    dist = (double)( -m*c3.x + c3.y + ( m*c2.x - c2.y ) )/std::sqrt(m*m + 1);
    /// Coseno del ángulo en B (Si es cercano a cero entonces es cercano a 90º)
    angbetween = calcAngleBetween(c1,c3, c2);
    if (std::abs(m) > 1e-12 && std::abs(dist) > 1e-12 && angbetween > 3.1416 )
    {
        pointT = c2;
        c2 = c1;
        c1 = pointT;
    }
    centros.push_back(c3);
    centros.push_back(c1);
    centros.push_back(c2);

    return centros;
}

cv::Point TestLabThread::complementaryPoint(cv::Point a, cv::Point b, cv::Point c)
{
    cv::Point v,w;
    v = b - a; w = c - a;
    return v+w+a;
}

cv::Point TestLabThread::growPoint(double grow, cv::Point a, cv::Point b, cv::Point c)
{
    cv::Point2d p1,p2,p3, v,w, p;

    v = a-b;
    v = v*(1/cv::norm(v));
    w = a-c;
    w = w*(1/cv::norm(w));

    p = a;

    p1 = p + grow*v;
    p2 = p + grow*w;
    p3 = complementaryPoint(a,p1,p2);

    return p3;
}

vector<cv::Point> TestLabThread::calcularActa(double grow, cv::Point a, cv::Point b, cv::Point c, cv::Point d)
{
    vector<cv::Point> acta;
    double dist;

    dist = std::max(cv::norm(a-b), cv::norm(a-c))*grow;

    acta.push_back(growPoint(dist,a,b,c));
    acta.push_back(growPoint(dist,b,a,d));
    acta.push_back(growPoint(dist,d,b,c));
    acta.push_back(growPoint(dist,c,a,d));

    return acta;
}

void TestLabThread::calcTransformationsPoints(cv::Point2f *vert, std::vector<cv::Point> centros, cv::Point2f *src, cv::Point2f *dest, int size)
{
    int i=1,j=0;
    //Ordenamos las esquinas
    double min;
    cv::Point2f centro;

    for (j=0; j < size; j++)
    {
        centro = centros[j];
        min = cv::norm(vert[0] - centro);
        src[j] = vert[0];

        for(i=1; i < 4; i++)
        {
            if ( cv::norm(vert[i]-centro) < min )
            {
                min = cv::norm(vert[i]-centro);
                src[j] = vert[i];
            }
        }
    }
    dest[0] = cv::Point2f(0.0,0.0);
    dest[1] = cv::Point2f(cv::norm(src[0]-src[1]),0.0);
    dest[2] = cv::Point2f(0.0,cv::norm(src[0]-src[2]));
    if (size == 4)
    {
        dest[3] = cv::Point2f(cv::norm(src[0]-src[1]),cv::norm(src[0]-src[2]));
    }
}

std::vector<std::vector<cv::Point>> TestLabThread::reCalculateSquares(std::vector<std::vector<cv::Point>> c, cv::Mat M)
{
    std::vector<std::vector<cv::Point>> cuadrados;
    std::vector<cv::Point> square;
    cv::Point2f p;
    int i=0;

    cuadrados.clear();

    for(i=0; i < c.size(); i++)
    {
        square.clear();
        cv::transform(c[i],square, M);
        cuadrados.push_back(square);
    }

    return cuadrados;
}

std::vector<std::vector<cv::Point>> TestLabThread::reCalculateSquaresP(std::vector<std::vector<cv::Point>> c, cv::Mat M)
{
    std::vector<std::vector<cv::Point>> cuadrados;
    std::vector<cv::Point2f> square, cuad;
    std::vector<cv::Point> squaref;
    int i=0,j;

    cuadrados.clear();

    //qDebug() << "profundida: " << (M.depth() == CV_32F || M.depth() == CV_64F);

    for(i=0; i < c.size(); i++)
    {
        square.clear();
        cuad.clear();
        for(j=0; j < c[i].size(); j++)
        {
            cuad.push_back(c[i].at(j));
        }

        cv::perspectiveTransform(cuad,square, M);
        for(j=0; j < square.size(); j++)
        {
            squaref.push_back(square.at(j));
        }
        cuadrados.push_back(squaref);
    }

    return cuadrados;
}

void TestLabThread::removeDuplicateSquares(std::vector<std::vector<cv::Point>> *quads)
{
    std::vector<cv::Point> centros, lado;
    std::vector<double> lados;

    int i ,j;

    for(i=0; i < quads->size(); i++)
    {
        centros.push_back(getCenter(quads->at(i)));
        lado.clear();
        lado.push_back((quads->at(i))[0]);
        lado.push_back((quads->at(i))[1]);
        lados.push_back(cv::arcLength(lado,false));
    }

    i=0;
    while(i < quads->size())
    {
        j=i+1;
        while(j < quads->size())
        {
            lado.clear();
            lado.push_back(centros.at(i));
            lado.push_back(centros.at(j));
            if (cv::arcLength(lado,false) < 2*lados.at(i))
            {
                lados.erase(lados.begin()+j);
                centros.erase(centros.begin()+j);
                quads->erase(quads->begin()+j);
            }
            else
            {
                j++;
            }
        }
        i++;
    }
}

vector<vector<cv::Point>> TestLabThread::obtainSquares(cv::Mat imagen, cv::Mat srcimg)
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx;
    vector<vector<cv::Point>> tempList, cuadros;
    vector<cv::Vec4i> hierarchy;
    cv::Mat mask, element;
    cv::Scalar cmean, stddev;
    int i;
    double area, areamin, areamax;

    cv::findContours(imagen,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    areamin = imagen.size().width*imagen.size().height*0.00024;
    areamax = imagen.size().width*imagen.size().height*0.0034;

    cuadros.clear();
    for(i=0; i < contours.size() ; i++)
    {
        tempList.clear();
        cv::approxPolyDP(contours[i],approx,0.025*(cv::arcLength(contours[i],true)),true);
        tempList.push_back(approx);
        area = cv::contourArea(approx);
        if (approx.size() == 4  && cv::isContourConvex(approx) && area > areamin && area < areamax && almostSquare(approx) )
        {
            mask = cv::Mat::zeros( srcimg.size(), CV_8U );
            cv::drawContours(mask,tempList, 0, 255, -1);
            element = cv::getStructuringElement(0,cv::Size(6,6));
            cv::morphologyEx(mask,mask,cv::MORPH_ERODE,element);
            cmean = cv::mean(srcimg,mask);
            cv::meanStdDev(srcimg,cmean,stddev,mask);
            if (stdDevisOK(stddev) && meanisOK(cmean)){
                //qDebug() << "area=" << area;
                cuadros.push_back(approx);
            }
        }
    }

    return cuadros;
}

void TestLabThread::obtainSquaresLog(cv::Mat imagen, cv::Mat &srcimg)
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx;
    vector<vector<cv::Point>> tempList;
    vector<cv::Vec4i> hierarchy;
    cv::Scalar color;
    cv::Point centro;
    cv::Mat mask, element;
    cv::Scalar cmean, stddev;
    int i;
    double area, areamin, areamax;

    QTextStream log(logF);
    QTextStream db(dbF);

    cv::findContours(imagen,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    areamin = imagen.size().width*imagen.size().height*0.00024;
    areamax = imagen.size().width*imagen.size().height*0.0034;

    for(i=0; i < contours.size() ; i++)
    {
        tempList.clear();
        cv::approxPolyDP(contours[i],approx,0.025*(cv::arcLength(contours[i],true)),true);
        tempList.push_back(approx);
        area = cv::contourArea(approx);
        if (approx.size() == 4  && cv::isContourConvex(approx) && area > areamin && area < areamax && almostSquare(approx) )
        {
            centro = getCenter(approx);
            color = cv::Scalar(43,25,220);
            mask = cv::Mat::zeros( srcimg.size(), CV_8U );
            cv::drawContours(mask,tempList, 0, 255, -1);
            element = cv::getStructuringElement(0,cv::Size(6,6));
            cv::morphologyEx(mask,mask,cv::MORPH_ERODE,element);
            cmean = cv::mean(srcimg,mask);
            cv::meanStdDev(srcimg,cmean,stddev,mask);
            log << "\tC("<< centro.x << "," << centro.y << ")\tArea: " << area << " ";
            if (stdDevisOK(stddev) && meanisOK(cmean)){
                color = cv::Scalar(25,220,62);
                log << "..... OK" << endl;
            }
            else
            {
                log << "..... FAIL" << endl;
            }
            cv::drawContours( srcimg, tempList, 0, color, 3, 8, hierarchy, 0, cv::Point() );
        }
    }
}

int TestLabThread::minimumStdDevSquares(std::vector<std::vector<cv::Point>> &cuadros, cv::Mat srcimg)
{
    int i, imin=-1;
    cv::Mat mask, element;
    cv::Scalar cmean, stddev;
    double dev, devmin=1e10;

    for(i=0; i < cuadros.size() ; i++)
    {
        mask = cv::Mat::zeros( srcimg.size(), CV_8U );
        cv::drawContours(mask,cuadros, i, 255, -1);
        element = cv::getStructuringElement(0,cv::Size(6,6));
        cv::morphologyEx(mask,mask,cv::MORPH_ERODE,element);
        cmean = cv::mean(srcimg,mask);
        cv::meanStdDev(srcimg,cmean,stddev,mask);
        dev = stddev[0]+stddev[1]+stddev[2];
        if (dev < devmin)
        {
            devmin = dev;
            imin = i;
        }
    }

    return imin;
}

cv::Point TestLabThread::calculatePerspectiveFourthPoint(std::vector<std::vector<cv::Point>> squares, std::vector<cv::Point> centers)
{
    cv::Point2d centro, A1, A2, B1, B2, lado, lado2, lado3, c1, c2;
    double cos1, cos2, alpha=0,tan=0, div=0;
    std::vector<cv::Point> cuadrado;
    bool bien=false;
    int idx[3],i,j;

    //buscamos los cuadrados ordenados;
    for(i=1; i < centers.size(); i++)
    {
        for(j=0; j < squares.size(); j++)
        {
            centro = getCenter(squares[j]);
            if (centro.x == centers[i].x && centro.y == centers[i].y)
                idx[i] = j;
        }
    }

    lado = centers[1]-centers[0];
    cuadrado = squares[idx[1]];

    lado2 = cuadrado[1]-cuadrado[0];
    lado3 = cuadrado[3]-cuadrado[0];
    cos1 = std::abs(lado.dot(lado2)/(cv::norm(cv::Mat(lado2))*cv::norm(cv::Mat(lado))));
    cos2 = std::abs(lado.dot(lado3)/(cv::norm(cv::Mat(lado3))*cv::norm(cv::Mat(lado))));
    c1 = cuadrado[2];
    if (cos1 < cos2)
    {
        c2 = cuadrado[3];
        A2 = lado2 + (c1-c2);
    }
    else
    {
        c2 = cuadrado[1];
        A2 = lado3 + (c1-c2);
    }
    A1 = centers[1];
    A2.x = A2.x/2;
    A2.y = A2.y/2;

    lado = centers[2]-centers[0];
    cuadrado = squares[idx[2]];

    lado2 = cuadrado[1]-cuadrado[0];
    lado3 = cuadrado[3]-cuadrado[0];
    cos1 = std::abs(lado.dot(lado2)/(cv::norm(cv::Mat(lado2))*cv::norm(cv::Mat(lado))));
    cos2 = std::abs(lado.dot(lado3)/(cv::norm(cv::Mat(lado3))*cv::norm(cv::Mat(lado))));
    c1 = cuadrado[2];
    if (cos1 < cos2)
    {
        c2 = cuadrado[3];
        B2 = lado2 + (c1-c2);
    }
    else
    {
        c2 = cuadrado[1];
        B2 = lado3 + (c1-c2);
    }
    B1 = centers[2];
    B2.x = B2.x/2;
    B2.y = B2.y/2;

    if (std::abs(A2.x+A2.y) > 1e-16 || std::abs(B2.x+B2.y) > 1e-16)
    {
        if (std::abs(B2.x) > 1e-16)
        {
            tan = B2.y/B2.x;
            div = A2.y - A2.x*tan;
            if (std::abs(div) > 1e-16)
            {
                alpha = (B1.y-A1.y - (B1.x-A1.x)*tan)/div;
                bien = true;
            }
        }
        else
        {
            tan = B2.x/B2.y;
            div = (A2.x - A2.y*tan);
            if (std::abs(div) > 1e-16)
            {
                alpha = (B1.x-A1.x - (B1.y - A1.y)*tan)/div;
                bien = true;
            }
        }
    }

    if (bien)
    {
        centro = A1 + alpha*A2;
    }
    else
    {
        centro = centers[0];
    }

    return centro;

}

std::vector<cv::Point> TestLabThread::calculatePerspectiveActa(std::vector<std::vector<cv::Point>> squares, std::vector<cv::Point> &centers, cv::Mat img)
{
    std::vector<cv::Point> acta, actamin;
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx;
    vector<cv::Vec4i> hierarchy;
    double area1, areamin, area;
    cv::Mat output, element;
    int i;
    QTextStream log(logF);

    cv::Point centro = calculatePerspectiveFourthPoint(squares,centers);

    if (centro.x != centers[0].x || centro.y != centers[0].y)
    {
        centers.push_back(centro);
        acta = calcularActa(0.017,centers[0], centers[1], centers[2], centers[3]);

        /*area1 = cv::contourArea(acta);
        //qDebug() << "Area O: " << area1;

        cv::cvtColor( img, output, CV_BGR2GRAY );
        cv::blur(output,output,cv::Size(5,5));
        cv::threshold(output,output,70,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
        element = cv::getStructuringElement(0,cv::Size(6,6));
        cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);
        cv::findContours(output,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

        areamin = -1;
        for(i=0; i < contours.size() ; i++)
        {
            cv::approxPolyDP(contours[i],approx,0.025*(cv::arcLength(contours[i],true)),true);
            area = cv::contourArea(approx);
            if (approx.size() == 4 && std::abs(area - area1) < 0.1*area1)
            {
                if (areamin < 0)
                {
                    areamin = area;
                    actamin = approx;
                }
                else
                {
                    if (area < areamin)
                    {
                        areamin = area;
                        actamin = approx;
                    }
                }
            }
        }
        if (areamin > 0)
        {
            acta = actamin;
            log << "\tArea calculada: " << area1 << " Area encontrada:" << areamin << endl;
        }*/

    }

    return acta;
}
