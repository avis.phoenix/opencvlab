#-------------------------------------------------
#
# Project created by QtCreator 2017-03-02T17:41:24
#
#-------------------------------------------------

QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OpenCVLab
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    optionsdlg.cpp \
    ventana.cpp \
    qcolorbutton.cpp \
    testlabthread.cpp

HEADERS  += mainwindow.h \
    optionsdlg.h \
    comandsstructure.h \
    ventana.h \
    qcolorbutton.h \
    testlabthread.h

FORMS += \
    optionsdlg.ui

win32:INCLUDEPATH += C:/opencv-3.2.0-mingw/include

win32:LIBS+= -LC:/opencv-3.2.0-mingw/x86/mingw/lib -llibopencv_core320 -llibopencv_imgcodecs320 -llibopencv_imgproc320 -llibopencv_photo320

linux-g++:LIBS+= -L/usr/lib64 -lopencv_core -lopencv_imgproc -lopencv_photo -lopencv_highgui

macx:INCLUDEPATH += /usr/local/include
macx:LIBS+= -L/usr/local/lib -lopencv_core -lopencv_imgproc -lopencv_imgcodecs -lopencv_photo -lopencv_highgui
